from django.contrib import admin
from django.urls import path
from core import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('login/', views.login_user),
    path('login/submit', views.submit_login),
    path('user/', views.user_page),
    path('cars/buy/', views.list_cars),
    path('car/filter/', views.filter_cars),
    path('api/car/filter/', views.get_list_cars),
    path('car/details/<id>/', views.car_details),
    path('cars/user/', views.list_user_cars),
    path('car/register/', views.register_car),
    path('car/register/submit', views.set_car),
    path('car/delete/<id>/', views.delete_car),
    path('user/register/', views.register_user),
    path('api/user/register/', views.set_user),
    path('api/validate_username/', views.validate_username),
    path('password/', views.change_password),
    path('api/password/', views.set_new_password)
]