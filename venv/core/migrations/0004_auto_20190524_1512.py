# Generated by Django 2.0.4 on 2019-05-24 18:12

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20190522_1711'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='price',
            field=models.FloatField(null=True, validators=[django.core.validators.MinValueValidator(0)]),
        ),
        migrations.AlterField(
            model_name='car',
            name='year',
            field=models.IntegerField(validators=[django.core.validators.MaxValueValidator(2019)]),
        ),
    ]
