from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator

class Car(models.Model):
    type = models.CharField(max_length=50)
    brand = models.CharField(max_length=50)
    year = models.IntegerField(validators=[MaxValueValidator(2019)])
    city = models.CharField(max_length=50)
    price = models.FloatField(validators=[MinValueValidator(0)], null=True)
    email = models.EmailField()
    phone_number = models.CharField(max_length=11)
    begin_date = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        db_table = 'car'
