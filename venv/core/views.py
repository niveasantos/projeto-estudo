from django.shortcuts import render, redirect
from django.template.defaultfilters import slugify
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .models import Car
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.http.response import JsonResponse
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import Group

def index(request):
    return redirect('/login/')

def login_user(request):
    return render(request, 'login.html')

def validate_username(request):
    username = request.GET.get('username')
    data = User.objects.filter(username__iexact=username).exists()
    return JsonResponse(data, safe=False)


@csrf_protect
def register_user(request):
    return render(request, 'register-user.html')

@csrf_protect
def set_user(request):
    username = request.POST.get('username')
    email = request.POST.get('email')
    password = request.POST.get('password')
    try:
        user = User.objects.create_user(username=username, email=email, password=password)
        user.save()
        return redirect('/login/')
    except IntegrityError:
        messages.error(request, 'Nome de usuário já existe')
        return redirect('/user/register/')



@csrf_protect
def submit_login(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/cars/buy/')
        else:
            messages.error(request, 'Senha ou usuário inválidos, tente novamente')
            return redirect('/login/')

@login_required(login_url='/login/')
def change_password(request):
    return render(request, 'change-password.html')

@csrf_protect
@login_required(login_url='/login/')
def set_new_password(request):
    password = request.POST.get('password')
    new_password = request.POST.get('new_password')
    user = User.objects.get(username=request.user)
    print(user)
    if check_password(password, user.password):
        user.set_password(new_password)
        user.save()
        return JsonResponse({'success': True})
    else:
        return JsonResponse({'message': 'Senha incorreta'})






@login_required(login_url='/login/')
def user_page(request):
    return render(request, 'user.html')

@login_required(login_url='/login/')
def list_cars(request):
    users_in_group = Group.objects.get(name='AdminGroup').user_set.all()
    car = Car.objects.filter(active=True)
    return render(request, 'list.html', {'car': car, 'users_in_group': users_in_group})

@login_required(login_url='/login/')
def list_user_cars(request):
    car = Car.objects.filter(active=True, user=request.user)
    return render(request, 'list.html', {'car': car})

@login_required(login_url='/login/')
def get_list_cars(request):
    city = request.GET.get('city')
    brand = request.GET.get('brand')
    type = request.GET.get('type')
    if brand is None:
        cars_list = list(Car.objects.filter(city=city, active=True).values_list('id', 'type', 'brand', 'year', 'price').order_by('type'))
    else:
        if type is None:
            cars_list = list(Car.objects.filter(city=city, brand=brand, active=True).values_list('id', 'type', 'brand', 'year', 'price').order_by('type'))
        else:
            cars_list = list(Car.objects.filter(city=city, brand=brand, type=type, active=True).values_list(
                                                                                'id', 'type', 'brand','year', 'price').order_by('type'))
    print(cars_list)
    return JsonResponse(cars_list, safe=False)

@csrf_protect
@login_required(login_url='/login/')
def filter_cars(request):
    car = list(Car.objects.filter(active=True))
    return render(request, 'filter-cars.html', {'cities': set(c.city for c in car),
                        'types': set(c.type for c in car)})

@login_required(login_url='/login/')
def car_details(request, id):
    car = Car.objects.get(active=True, id=id)
    return render(request, 'details.html', {'car': car})

@login_required(login_url='/login/')
def register_car(request):
    car_id = request.GET.get('id')
    if car_id:
        car = Car.objects.get(id=car_id)
        if car.user == request.user:
            return render(request, 'register-car.html', {'car': car})
    return render(request, 'register-car.html')

@login_required(login_url='/login/')
@csrf_protect
def set_car(request):
    print(123)
    type = request.POST.get('type')
    brand = request.POST.get('brand')
    year = request.POST.get('year')
    city = request.POST.get('city')
    price = request.POST.get('price')
    email = request.POST.get('email')
    phone_number = request.POST.get('phone_number')
    car_id = request.POST.get('car-id')
    user = request.user
    if car_id:
        car = Car.objects.get(id=car_id)
        if user == car.user:
            car.type = type
            car.brand = brand
            car.year = year
            car.city = city
            car.price = price
            car.email = email
            car.phone_number = phone_number
            car.save()
            return JsonResponse({'success': True, 'update': True, 'car_id': car.id})
        else:
            return JsonResponse({'succes': False})
    else:
        car = Car.objects.create(type=type, brand=brand, year=year, city=city, price=price,
                             email=email, phone_number=phone_number, user=user)
        return JsonResponse({'success': True, 'create': True, 'car_id': car.id})



@login_required(login_url='/login/')
def delete_car(request, id):
    car = Car.objects.get(id=id)
    if car.user == request.user:
        car.delete()
    return redirect('/cars/user/')


